/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  *
 ******************************************************************************
 *  $File Name$       : Flash.h                                               *
 *  $Author$          : ChipON AE/FAE Group                                   *
 *  $Data$            : 2021-07-8                                             *
 *  $Hard Version     : KF32A156-MINI-EVB_V1.2                                *
 *  $Description$     : This file provides the operation Flash  header file   *
 ******************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 *
 *  All rights reserved.                                                      *
 *                                                                            *
 *  This software is copyright protected and proprietary to                    *
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 *
 ******************************************************************************
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *                     		REVISON HISTORY                               	  *
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *  Data       Version  Author        Description                             *
 *  ~~~~~~~~~~ ~~~~~~~~ ~~~~~~~~~~~~  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *  2021-07-08 00.02.00 FAE Group     Version 2.0 update                      *
 *                                                                            *
 *                                                                            *
 *****************************************************************************/

#ifndef _FLASH_H_
#define _FLASH_H_

/******************************************************************************
*                      Functional defineition                                 *
******************************************************************************/
void FlashReadNByte(unsigned int Address, unsigned int Length, unsigned char *Buffers);
uint32_t FlashEraseNPage(unsigned int Address, unsigned int Length);
uint32_t FlashWriteNKBytes(unsigned int Address, unsigned int Length, unsigned char *Buffers);
uint32_t FlashEEWriteNBytes(unsigned int Address, unsigned int Length, unsigned char *Buffers);
uint32_t FlashCheckNBytes(unsigned int Address, unsigned int Length, unsigned char *Buffers);
uint32_t FlashCFGUserWrite(unsigned int Address,unsigned int Length,unsigned int *Buffers);
uint32_t FlashCFGUserRead(unsigned int Address,unsigned int Length,unsigned int *Buffers);
#endif /* FLASH_H_ */
